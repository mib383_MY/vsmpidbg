﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.VCProjectEngine;
using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO.Packaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.UI.Design.WebControls;
using Task = System.Threading.Tasks.Task;

namespace vsmpidbg
{
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(VsmpidbgPackage.PackageGuidString)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    public sealed class VsmpidbgPackage : AsyncPackage
    {
        public const string PackageGuidString = "999a9200-d097-4587-a19d-d43f8f08f2ce";
        static class IdList
        {
            //public const uint id_menu_group = 91;
            //public const uint id_toolbar = 92;
            //public const uint id_toolbar_group = 93;

            public const uint id_btn_on_play = 11;
            public const uint id_btn_on_stop = 12;

            public const uint id_cbo_on_value = 21;
            public const uint id_cbo_on_get_list = 22;

            //public const uint id_ico_play = 1;
            //public const uint id_ico_stop = 2;
        };

        static DTE dte;
        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            await this.JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);

            dte = await GetServiceAsync(typeof(DTE)).ConfigureAwait(true) as DTE ?? throw (new NullReferenceException("Unable to get DTE service"));

            var mcs = await GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService ?? throw (new NullReferenceException("Unable to get OleMenuCommandService service"));
            var guid = new Guid(PackageGuidString);

            var cboOnValueCmdId = new CommandID(guid, (int)IdList.id_cbo_on_value);
            var cboOnListCmdId = new CommandID(guid, (int)IdList.id_cbo_on_get_list);
            var btnOnPlayCmdId = new CommandID(guid, (int)IdList.id_btn_on_play);
            var btnOnStopCmdId = new CommandID(guid, (int)IdList.id_btn_on_stop);

            mcs.AddCommand(new MenuCommand(this.OnBtnPlay, btnOnPlayCmdId));
            mcs.AddCommand(new MenuCommand(this.OnBtnStop, btnOnStopCmdId));
            mcs.AddCommand(new OleMenuCommand(this.OnCboValue, cboOnValueCmdId));
            mcs.AddCommand(new OleMenuCommand(this.OnCboGetList, cboOnListCmdId));
        }

        struct ProjectProperties
        {
            public string exe_path;
            public string exe_name;
            public string exe_dir;
        }

        struct MpiProperties
        {
            public string mpiexec;
            public string mpi_path;
        }

        ProjectProperties ProjProps = new ProjectProperties();
        MpiProperties MpiProps = new MpiProperties();

        private bool InitMpiProperties(string mpiexec)
        {
            string mpi_path = Environment.GetEnvironmentVariable("PATH")
                .Split(';')
                .Where(s => System.IO.File.Exists(System.IO.Path.Combine(s, mpiexec)))
                .FirstOrDefault();
            if (!System.IO.File.Exists(System.IO.Path.Combine(mpi_path, mpiexec)))
                return false;
            MpiProps.mpiexec = mpiexec;
            MpiProps.mpi_path = mpi_path;
            return true;
        }

        private VCConfiguration GetProjectConfig()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var project = dte.Solution.Projects.Item(1);
            if (project == null)
                return null;

            VCProject vcproject = (VCProject)project.Object;
            if (vcproject == null)
                return null;

            return vcproject.ActiveConfiguration;
        }
        private bool InitProjectProperties()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var config = GetProjectConfig();
            if (config == null)
                return false;

            ProjProps.exe_path = config.PrimaryOutput;
            ProjProps.exe_name = System.IO.Path.GetFileName(ProjProps.exe_path);
            ProjProps.exe_dir = System.IO.Path.GetDirectoryName(ProjProps.exe_path);
            return true;
        }

        private bool TerminateAll(string exe_name)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var proc_list = dte.Debugger.LocalProcesses;
            foreach (EnvDTE.Process proc in proc_list)
            {
                if (proc.Name.ToString().EndsWith(exe_name))
                {
                    proc.Terminate(false);
                }
            }
            return true;
        }

        private bool CheckDebugConfig()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            string debug_release_config = dte.Solution.SolutionBuild.ActiveConfiguration.Name;
            return debug_release_config.Equals("Debug");
        }

        private bool BuildProject()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var config = GetProjectConfig();
            if (config == null)
                return false;

            var cpptool = ((IVCCollection)config.Tools).Item("VCCLCompilerTool") as VCCLCompilerTool;

            var bkp = cpptool.AdditionalOptions;
            cpptool.AdditionalOptions += "/DMPI_DEBUG";

            dte.Solution.SolutionBuild.Build(true);
            cpptool.AdditionalOptions = bkp;
            if (dte.Solution.SolutionBuild.LastBuildInfo != 0)
                return false;
            return true;
        }

        private bool StartMpi()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(System.IO.Path.Combine(MpiProps.mpi_path, MpiProps.mpiexec))
            {
                Arguments = "-n " + Properties.Settings.Default.DefaultMpiThreadCount.ToString() + " " + ProjProps.exe_path,

                WorkingDirectory = ProjProps.exe_dir
            };

            var mpi_process = System.Diagnostics.Process.Start(startInfo);

            if (mpi_process == null)
                return false;

            return true;
        }

        private void DetachAll(string exe_name)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            foreach (EnvDTE.Process proc in dte.Debugger.LocalProcesses)
            {
                if (proc.Name.ToString().EndsWith(exe_name))
                {
                    proc.Detach(false);
                }
            }
        }

        private int AttachAll(string exe_name)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            int attached = 0;
            foreach (EnvDTE.Process proc in dte.Debugger.LocalProcesses)
            {
                if (proc.Name.ToString().EndsWith(exe_name))
                {
                    proc.Attach();
                    attached++;
                }
            }
            return attached;
        }

        private void OnBtnStop(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            if (!InitProjectProperties())
                throw (new NullReferenceException("Unable to init project properties! Unknown reason!"));
            TerminateAll(ProjProps.exe_name);
        }
        private void OnBtnPlay(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            if (!CheckDebugConfig())
                throw (new Exception("Select \"Debug\" instead \"Release\" configuration to start MPI debugging!"));

            if(!BuildProject())
                throw (new Exception("Error while building project! Fix all errors and try again!"));

            if (!InitProjectProperties())
                throw (new NullReferenceException("Unable to init project properties! Unknown reason!"));

            if(!InitMpiProperties("mpiexec.exe"))
                throw (new Exception("Error while searching MPI! Ensure path to mpiexec is in you system %PATH% variable"));


            TerminateAll(ProjProps.exe_name);

            if (!StartMpi())
                throw (new Exception("Error while starting MPI! Unknown reason!"));


            DetachAll(ProjProps.exe_name);
            int attached = AttachAll(ProjProps.exe_name);

            if (attached != Properties.Settings.Default.DefaultMpiThreadCount)
                throw (new Exception("Error while attaching processes to debugger! Possible reason: you forgot to include part: #ifdef MPI_DEBUG..."));
        }

        private readonly int[] predefinedThreadCounts = { 1, 2, 4, 8, 16, 32 };

        int currentValue_ = Properties.Settings.Default.DefaultMpiThreadCount;
        private int CurrentValue
        {
            get { return currentValue_; }
            set { currentValue_ = value; Properties.Settings.Default.DefaultMpiThreadCount = value; Properties.Settings.Default.Save(); }
        }
        

        private void OnCboValue(object sender, EventArgs e)
        {
            if ((null == e) || (e == EventArgs.Empty))
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException("Resources.EventArgsRequired")); // force an exception to be thrown
            }


            if (e is OleMenuCmdEventArgs eventArgs)
            {
                object input = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (vOut != IntPtr.Zero && input != null)
                {
                    throw (new ArgumentException("Resources.BothInOutParamsIllegal")); // force an exception to be thrown
                }
                else if (vOut != IntPtr.Zero)
                {
                    Marshal.GetNativeVariantForObject(CurrentValue.ToString(), vOut);
                }
                else if (input != null)
                {
                    // new value was selected or typed in
                    string inputString = input.ToString();

                    bool parse_result = int.TryParse(inputString, out int parsed);
                    if (parse_result && parsed > 0)
                    {
                        CurrentValue = parsed;
                    }
                    else
                    {
                        //throw (new ArgumentException("Cannot parse to positive int"));
                    }
                }
                else
                {
                    // We should never get here
                    throw (new ArgumentException("Resources.InOutParamCantBeNULL")); // force an exception to be thrown
                }
            }
            else
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException("Resources.EventArgsRequired")); // force an exception to be thrown
            }
        }

        private void OnCboGetList(object sender, EventArgs e)
        {
            if ((null == e) || (e == EventArgs.Empty))
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentNullException("Resources.EventArgsRequired")); // force an exception to be thrown
            }


            if (e is OleMenuCmdEventArgs eventArgs)
            {
                object inParam = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (inParam != null)
                {
                    throw (new ArgumentException("Resources.InParamIllegal")); // force an exception to be thrown
                }
                else if (vOut != IntPtr.Zero)
                {
                    string[] predefinedThreadCountsStrs = new string[predefinedThreadCounts.Length];
                    int counter = 0;

                    foreach (var _ in predefinedThreadCounts)
                        predefinedThreadCountsStrs[counter++] = _.ToString();

                    Marshal.GetNativeVariantForObject(predefinedThreadCountsStrs, vOut);
                }
                else
                {
                    throw (new ArgumentException("Resources.OutParamRequired")); // force an exception to be thrown
                }
            }
        }

        public int GetMpiThreadCount()
        {
            return currentValue_;
        }
        }
    }

