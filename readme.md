# MPI Debug Extension for Visual Studio 2022

## Intro

This extension tries to return back old functionality of Microsoft `MPI Cluster Debugger` for modern Microsoft Visual Studio 2022 on Windows systems. The idea is to start `mpiexec` and then to attach all the processes to the debugger. This extension automates the building, starting and attaching process using friendly gui addition to Visual Studio 2022.

## Requirements

- Windows 10/11 (tested on Win10x64);
- Microsoft Visual Studio 2022 with Microsoft C++ build tools (tested on Community Edition);
- Off course [MPI](https://learn.microsoft.com/en-us/message-passing-interface/microsoft-mpi) should be installed. Also `mpiexec.exe` should be in `%PATH%` variable which happens by default while MPI installation.


## Getting started

1. Install extension `*.vsix` file from the [Release](https://gitlab.com/mib383_MY/vsmpidbg/-/releases) page.
2. In VS2022 application go to: `View->Toolbar` and choose `MPI Debug Menu`:
   
   ![](other/view_toolbars.png)

3. Then you will see the toolbar:
   
   ![](other/mpi_debug_menu.png)

   where
   - combobox allows you to choose mpi process count (`-n` option). Let `n=6`. And our `.exe` name is `exe_name.exe`;
   - `play` button builds current project, starts `mpiexec -n 6 exe_name.exe` and attaches the debugger to all the processes;
   - `stop` button detaches the debugger and terminates all `exe_name.exe` in system;
   - course path to `exe_name.exe` is aumatically determines from VS2022 project internal variables.
4. I do not know the way to attach the debugger on the very start of the program, thats why we need to pause it until all attaching actions will done. For this reason we should slightly modify the source code:

    right after `rank` variable will be known (for example after):

    ```c++
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    ```

    we should insert something like this:

    ```c++
    #ifdef MPI_DEBUG
        if (rank == 0) {
	        printf("MPI debugging started, enter any char to continue...");
	        fflush(stdout);
	        getchar();
        }     
        MPI_Barrier(MPI_COMM_WORLD);
    #endif
    ```
    Here `MPI_DEBUG` macro is automatically adds to compiler flags when building by `play` will start.

Youtube video: `TO DO`

## Possible problems

`TO DO`

## Aknowledgments

I hope this extension will help somebody like it helps me. If you want you can thank me with a donation:

|||
|:---:|:---:|
|BTC|`bc1qhgw7mps46kscjd20csvd6fn5dux75z6xnnuzyk`|
|ETH|`0xDBB08FC7f588D8dbEF7eB88DACAdA23c3Aaa9941`|
|USDT (TRC20)|`TXH3NZKoeKGHM1c93mTUB69z9B1wLLNTvJ`|